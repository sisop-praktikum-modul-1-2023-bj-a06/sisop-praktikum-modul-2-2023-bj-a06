#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <syslog.h>
#include <errno.h>
#include <fcntl.h>
#include <unistd.h>
#include <time.h>
#include <string.h>

int main(){
    bool bhour = false, bmin = false, bsec=false;
    int cronhour, cronmin, cronsec, status;
    char scronhour[3], scronmin[3], scronsec[3], path[100];
    scanf("%s %s %s %s", scronhour, scronmin, scronsec, path);
    if(strcmp(scronhour, "*")!=0){
        cronhour = atoi(scronhour);
        if(!(cronhour >=0 || cronhour <=23)){
            puts ("Error Hour Argument");
            return 0;
        }
    } else {
        bhour = true;
    }
    if (strcmp(scronmin, "*")!=0){
        cronmin = atoi(scronmin);
        if(!(cronmin >=0 || cronmin <=59)){
            puts ("Error Minute Argument");
            return 0;
        }
    } else{
        bmin = true;
    }
    if (strcmp(scronsec, "*")!=0){
        cronsec = atoi(scronsec);
        if(!(cronsec >=0 || cronsec <=59)){
            puts ("Error Minute Argument");
            return 0;
        }
    } else{
        bsec=true;
    }

    pid_t in_pid, in_sid;
    in_pid=fork();
    if (in_pid<0){
        exit(EXIT_FAILURE);
    }
    if(in_pid>0){
        exit(EXIT_SUCCESS);
    }
    umask(0);
    in_sid=setsid();
    if(in_sid<0){
        exit(EXIT_FAILURE);
    }
    if((chdir("/"))<0){
        exit(EXIT_FAILURE);
    }
    close(STDIN_FILENO);
    close(STDERR_FILENO);
    close(STDOUT_FILENO);
    while (1){
        time_t t = time (NULL);
        struct tm tm = *localtime(&t);
        if((tm.tm_hour == cronhour || bhour) && (tm.tm_min == cronmin || bmin) && (tm.tm_sec == cronsec || bsec)){
            pid_t pid;
            pid = fork();
            if(pid==0){
                execl("/bin/bash", "bash", path, NULL);
            } 
        }
        sleep(1);
    }
    return 0;
}