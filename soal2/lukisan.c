#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>
#include <string.h>
#include <sys/stat.h>
#include <curl/curl.h>
#include <signal.h>

#define NUM_IMAGES 15
#define IMAGE_DOWNLOAD_INTERVAL 5
#define FOLDER_CREATE_INTERVAL 30


void killer_a(){
    FILE *killer_file = fopen("killer.c", "w");
    fprintf(killer_file, "#include <stdio.h>\n#include <stdlib.h>\n#include <sys/stat.h>\n#include <unistd.h>\n#include <curl/curl.h>\nvoid main(){\npid_t pid;\npid = fork();\nif (pid==0){\nchar *argv[]= {\"killall\",\"lukisan\", NULL};\nexecv(\"/bin/killall\", argv);\n}else{\nchar *argv[]= {\"rm\", \"killer\", NULL};\nexecv(\"/bin/rm\", argv);\n}\n}");
    fclose(killer_file);
    char *argv[]={"gcc", "killer.c", "-o", "killer", NULL};
    pid_t pid;
    pid =fork();
    if(pid==0){
        execv("/bin/gcc", argv);
    }
}

void killer_b(){
    FILE *killer_file = fopen("killer.c", "w");
    fprintf(killer_file, "#include <stdio.h>\n#include <stdlib.h>\n#include <sys/stat.h>\n#include <unistd.h>\n#include <curl/curl.h>\nvoid main(){\npid_t pid;\npid = fork();\nif (pid==0){\nchar *argv[]= {\"pkill\", \"-9\",\"-o\",\"lukisan\" , NULL};\nexecv(\"/bin/pkill\", argv);\n}else{\nchar *argv[]= {\"rm\", \"killer\", NULL};\nexecv(\"/bin/rm\", argv);\n}\n}");
    fclose(killer_file);
    char *argv[]={"gcc", "killer.c", "-o", "killer", NULL};
    pid_t pid;
    pid =fork();
    if(pid==0){
        execv("/bin/gcc", argv);
    }
}

int main(int argc, char *argv[]) {
    time_t now;
    struct tm *tm_info;
    char folder_name[100], file_name[100], url[50], file_path[200];
    int i;
    char *mode = argv[1]+1;
    if(strcmp(mode,"a")==0){
        killer_a();
    } else if(strcmp(mode,"b")==0){
        killer_b();
    } else{
        puts("please choose -a | -b");
        return 0;
    }
    pid_t in_pid, in_sid;
    in_pid=fork();
    if (in_pid<0){
        exit(EXIT_FAILURE);
    }
    if(in_pid>0){
        exit(EXIT_SUCCESS);
    }
    umask(0);
    in_sid=setsid();
    if(in_sid<0){
        exit(EXIT_FAILURE);
    }

    if((chdir("/"))<0){
        exit(EXIT_FAILURE);
    }

    close(STDIN_FILENO);
    close(STDOUT_FILENO);
    close(STDERR_FILENO);
    
    while (1) {
        //ganti path working directory
        chdir("/home/victorgg345/Documents/SistemOperasi/Soal2");
        time(&now);
        tm_info = localtime(&now);
        strftime(folder_name, sizeof(folder_name), "%Y-%m-%d_%H:%M:%S", tm_info);
        mkdir(folder_name, 0700);
        pid_t pid;
        pid = fork();
        if(pid==0){
            for (i = 0; i < NUM_IMAGES; i++) {
                time(&now);
                tm_info = localtime(&now);
                strftime(file_name, sizeof(file_name), "%Y-%m-%d_%H:%M:%S", tm_info);
                sprintf(url, "https://picsum.photos/%d", (int)now % 1000 + 50);
                sprintf(file_path, "%s/%s", folder_name, file_name);
                puts(file_path);
                pid_t pid2;
                pid2= fork();
                char *argv[]={"wget", "-O" , file_path, url , NULL};
                if(pid2==0){
                    execv("/bin/wget", argv);
                }
                sleep(IMAGE_DOWNLOAD_INTERVAL);
            }
            pid_t pidzip;
            pidzip = fork();
            if(pidzip==0){
                char *argv[]={"rm", "-rf",folder_name, NULL};
                execv("/bin/rm", argv);
            } else{
                char zipname[110];
                sprintf(zipname, "%s.zip", folder_name);
                char *argv[]={"zip", zipname, folder_name, NULL};
                execv("/bin/zip", argv);
            }
            exit(EXIT_SUCCESS);
        } 
        sleep(FOLDER_CREATE_INTERVAL);
    }

    return 0;
}