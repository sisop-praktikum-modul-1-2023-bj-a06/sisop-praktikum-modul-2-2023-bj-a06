# sisop-praktikum-modul-2-2023-BJ-A06

## Laporan Pengerjaan Soal Shift Modul 2 Praktikum Sistem Operasi

### Daftar Anggota :
1. Lihardo Marson Purba - 5025211238
2. Farhan Dwi Putra     - 5025211093
3. Victor Gustinova     - 5025211159

### Soal 1
Pada soal 1 kita diminta pertama kali untuk mendownload zip file dari link yang sudah tertera. untuk itu pada file binatang.c kita tuliskan code sebagai berikut.
``` sh
    pid_t c_pid = fork();
    if (c_pid ==0){
    char *argv[7] = {"wget","-P","/home/lihardo04/Desktop/soal1","-O","binatang.zip", "https://drive.google.com/uc?       export=download&id=1oDgj5kSiDO0tlyS7-20uz7t20X3atwrq", NULL};
    execv("/usr/bin/wget", argv);
      }
    else {
    wait(NULL);
    printf("File downloaded successfully!\n");
    }
```
- [ ]  Saya memngimplementasikannya dengan mengenerate child process dan melakukan wait pada parent process, dengan artian bahwa program tetap berjalan secara linear.

- Sub_soal 1 -> melakukan unzip pada zip file yang telah didwoanload.
``` sh
    c_pid = fork();
    if (c_pid ==0) {
      char *argv[5] = {"unzip", "binatang.zip", "-d", "binatang", NULL};
      execv("/usr/bin/unzip", argv);
    }
    else {
      wait (NULL);
      printf ("Unzip file done!\n");
    }
```
- [ ]  Dari statement code diatas, program melakukan unzip dari zip file dan menamai filenya menjadi directory binatang.

- Sub_soal 2 -> Melakukan pengambilan file .png dari directory binatang secara random.
``` sh
    char command[256];  
    sprintf(command, "cp \"%s\" /home/lihardo04/Desktop/soal1/current_u_spv/", "$(shuf -n 1 -e /home/lihardo04/Desktop/soal1/binatang/*)");
    strcat(command, "/");
    strcat(command, "$(basename \"$command\")");
    int ret = system(command);
    if (ret == -1) {
        perror("system failed");
        exit(EXIT_FAILURE); 
    } else if (WEXITSTATUS(ret) != 0) {
        printf("Command failed\n");
    } else {
        printf("Command succeeded\n");
    }
```
- [ ]  Dari statement code diatas kita melakukannya dengan function "system" dimana perintahnya terdapat dalam variable command.

- Sub_soal 3 -> melakukan pembuatan directory HewanDarat, HewanAmphibi, dan HewanAir serta melakukan filter dan pemindahan file dari directory binatang ke directory yang sesuai dengan jenis binatang apa dari gambar yang direpresentasikan file - file yang ada dalam direcotry binatang.
 ``` sh
    system ("mkdir /home/lihardo04/Desktop/soal1/HewanDarat");
    system ("mkdir /home/lihardo04/Desktop/soal1/HewanAmphibi");
    system ("mkdir /home/lihardo04/Desktop/soal1/HewanAir");
```
- [ ]  Statement diatas melakukan pembuatan directory - directory yang ingin dibuat.
 ``` sh
    system("find /home/lihardo04/Desktop/soal1/binatang -type f -name \"*air*\" -exec mv {} /home/lihardo04/Desktop/soal1/HewanAir \\;");
    system("find /home/lihardo04/Desktop/soal1/binatang -type f -name \"*darat*\" -exec mv {} /home/lihardo04/Desktop/soal1/HewanDarat \\;");
    system("find /home/lihardo04/Desktop/soal1/binatang -type f -name \"*amphibi*\" -exec mv {} /home/lihardo04/Desktop/soal1/HewanAmphibi \\;");
```
- [ ]  statement diatas melakukan filter serte memindahkan file - file sesuai klasifikasi mereka ke masing - masing directory yang sudah dibuat diatas.

- Sub_soal 4 -> Melakukan zip pada masing - masing direcotry HewanDarat, HewanAmphibi, dan HewanAir. 
 ``` sh
    for (int i = 1; i < 4; i++) {
        char key_word[30];
        if (i == 1) {
            strcpy(key_word, "HewanAir");
        }
        else if (i == 2) {
            strcpy(key_word, "HewanDarat");
        }
        else if (i == 3) {
            strcpy(key_word, "HewanAmphibi");
        }

        char zip_name[40];
        sprintf(zip_name, "%s.zip", key_word);

        c_pid = fork();
        if (c_pid == 0) {
            char *argv[10] = {"zip", "-r", zip_name, key_word, "-jr","-x", "*.zip", "-x", ".", NULL};
            chdir("/home/lihardo04/Desktop/soal1");  // Change working directory to the parent directory
            execv("/usr/bin/zip", argv);
        }
        else {
            int status;
            waitpid(c_pid, &status, 0);
            if (WIFEXITED(status) && WEXITSTATUS(status) == 0) {
                printf("%s file done!\n", zip_name);
            } else {
                fprintf(stderr, "zip error: %s\n", zip_name);
            }
        }
        
        chdir("/home/lihardo04/Desktop/soal1");
        c_pid = fork();
          if (c_pid == 0){
              char *argv[4] = {"rm","-r", key_word,NULL};
              execv("/usr/bin/rm", argv);
          }
          else {
            wait (NULL);
            printf ("delete directory done!\n");
      }
    }
```
- [ ] program diatas melakukan zip file secara otomatis dengan generate command secara berulang di dalam loop for. Program diatas juga melakukan penghapusan direcotry file yang sudah di-zip di setiap perulangannya.

<br>
> Sebelum menjalankan program binatang.c yang sudah di compile menjadi get.exe
![Screenshot_2023-04-08_124236](/uploads/779459b2145c21ba2366864c82ca3764/Screenshot_2023-04-08_124236.png)
<br>

<br>
> Setelah menjalankan program get.exe
![Screenshot_2023-04-08_124452](/uploads/06977904745b011fab25805679e1e703/Screenshot_2023-04-08_124452.png)
<br>

#### Kendala dan Catatan Soal 1
Kendala sempat terjadi dalam pembuatan process dan bagaimana menghentikan agar process tidak berlanjut ke keseluruhan parent process.

### Soal 2
- Pertama-tama, buatlah sebuah folder khusus, yang dalamnya terdapat sebuah program C yang per 30 detik membuat sebuah folder dengan nama timestamp [YYYY-MM-dd_HH:mm:ss].
``` sh
while (1) {
        time(&now);
        tm_info = localtime(&now);
        strftime(folder_name, sizeof(folder_name), "%Y-%m-%d_%H:%M:%S", tm_info);
        mkdir(folder_name, 0700);
        sleep(FOLDER_CREATE_INTERVAL);
```
- [ ]  Dapatkan nilai time dan buat nama folder dengan strftime. Folder dibuat dengan mkdir dan sleep setiap 30 detik.

- Tiap-tiap folder lalu diisi dengan 15 gambar yang di download dari https://picsum.photos/ , dimana tiap gambar di download setiap 5 detik. Tiap gambar berbentuk persegi dengan ukuran (t%1000)+50 piksel dimana t adalah detik Epoch Unix. Gambar tersebut diberi nama dengan format timestamp [YYYY-mm-dd_HH:mm:ss].

- Agar rapi, setelah sebuah folder telah terisi oleh 15 gambar, folder akan di zip dan folder akan di delete(sehingga hanya menyisakan .zip).

``` sh
    pid_t pid;
    pid = fork();
    if(pid==0){
        for (i = 0; i < NUM_IMAGES; i++) {
            time(&now);
            tm_info = localtime(&now);
            strftime(file_name, sizeof(file_name), "%Y-%m-%d_%H:%M:%S", tm_info);
            sprintf(url, "https://picsum.photos/%d", (int)now % 1000 + 50);
            sprintf(file_path, "%s/%s", folder_name, file_name);
            puts(file_path);
            pid_t pid2;
            pid2= fork();
            char *argv[]={"wget", "-O" , file_path, url , NULL};
            if(pid2==0){
                execv("/bin/wget", argv);
            }
            sleep(IMAGE_DOWNLOAD_INTERVAL);
        }
        pid_t pidzip;
        pidzip = fork();
        if(pidzip==0){
            char *argv[]={"rm", "-rf",folder_name, NULL};
            execv("/bin/rm", argv);
        } else{
            char zipname[110];
            sprintf(zipname, "%s.zip", folder_name);
            char *argv[]={"zip", zipname, folder_name, NULL};
            execv("/bin/zip", argv);
        }
        exit(EXIT_SUCCESS);
    } 

```
- [ ] Didalam daemon akan dilakukan fork kembali. Dilakukan for untuk mendownload gambar sebanyak 15 kali. Digunakan strftime untuk mendapatkan nama file, sprintf untuk nama url dan file_path. Lalu dilakukan fork lagi untuk menjalankan wget.

- [ ] Didalam daemon, setelah fork pertama dilakukan fork kedua untuk menjalankan zip(parent) lalu rm(child).


- Karena takut program tersebut lepas kendali, Sucipto ingin program tersebut men-generate sebuah program "killer" yang siap di run(executable) untuk menterminasi semua operasi program tersebut. Setelah di run, program yang menterminasi ini lalu akan mendelete dirinya sendiri.
- Buatlah program utama bisa dirun dalam dua mode, yaitu MODE_A dan MODE_B. untuk mengaktifkan MODE_A, program harus dijalankan dengan argumen -a. Untuk MODE_B, program harus dijalankan dengan argumen -b. Ketika dijalankan dalam MODE_A, program utama akan langsung menghentikan semua operasinya ketika program killer dijalankan. Untuk MODE_B, ketika program killer dijalankan, program utama akan berhenti tapi membiarkan proses di setiap folder yang masih berjalan sampai selesai(semua folder terisi gambar, terzip lalu di delete).

```sh
char *mode = argv[1]+1;
if(strcmp(mode,"a")==0){
    killer_a();
} else if(strcmp(mode,"b")==0){
    killer_b();
} else{
    puts("please choose -a | -b");
    return 0;
}
```
- [ ] Untuk memilih mode maka akan diambil argumen dan akan dijalankan fungsi untuk masing masing mode.
```sh
void killer_a(){
    FILE *killer_file = fopen("killer.c", "w");
    fprintf(killer_file, "#include <stdio.h>\n#include <stdlib.h>\n#include <sys/stat.h>\n#include <unistd.h>\n#include <curl/curl.h>\nvoid main(){\npid_t pid;\npid = fork();\nif (pid==0){\nchar *argv[]= {\"killall\",\"lukisan\", NULL};\nexecv(\"/bin/killall\", argv);\n}else{\nchar *argv[]= {\"rm\", \"killer\", NULL};\nexecv(\"/bin/rm\", argv);\n}\n}");
    fclose(killer_file);
    char *argv[]={"gcc", "killer.c", "-o", "killer", NULL};
    pid_t pid;
    pid =fork();
    if(pid==0){
        execv("/bin/gcc", argv);
    }
}


void killer_b(){
    FILE *killer_file = fopen("killer.c", "w");
    fprintf(killer_file, "#include <stdio.h>\n#include <stdlib.h>\n#include <sys/stat.h>\n#include <unistd.h>\n#include <curl/curl.h>\nvoid main(){\npid_t pid;\npid = fork();\nif (pid==0){\nchar *argv[]= {\"pkill\", \"-9\",\"-o\",\"lukisan\" , NULL};\nexecv(\"/bin/pkill\", argv);\n}else{\nchar *argv[]= {\"rm\", \"killer\", NULL};\nexecv(\"/bin/rm\", argv);\n}\n}");
    fclose(killer_file);
    char *argv[]={"gcc", "killer.c", "-o", "killer", NULL};
    pid_t pid;
    pid =fork();
    if(pid==0){
        execv("/bin/gcc", argv);
    }
}
```

- [ ] Fungsi killer akan membuat sebuah file c baru sesuai dengan ketentuan yang diminta. File tersebut akan dicompile dan nantinya dapat dijalankan.

- [ ] Killer a menggunakan killall untuk mengkill semua proses yang bernama lukisan dan killer b menggunakan pkill untuk mengkill proses pertama bernama lukisan yang dijalankan . Setelah itu killer akan menjalankan rm pada dirinya sendiri.

### Soal 3
- Sub_soal 1 -> Melakukan download file player.zip dari link gdrive yang disediakan, serta melakukan unzip pada player.zip yang telah didownload.
```sh
    pid_t c_pid = fork();
    if (c_pid ==0){
    char *argv[8] = {"wget","--no-check-certificate","-P","/home/lihardo04/Desktop/soal3","-O","players.zip", "https://drive.google.com/uc?export=download&id=1zEAneJ1-0sOgt13R1gL4i1ONWfKAtwBF", NULL};
    execv("/usr/bin/wget", argv);
      }
    else {
    wait(NULL);
    printf("File downloaded successfully!\n");
    }
    
    c_pid = fork();
    if (c_pid ==0) {
      char *argv[6] = {"unzip", "players.zip", "players/*","-O","players", NULL};
      execv("/usr/bin/unzip", argv);
    }
    else {
      wait (NULL);
      printf ("Unzip file done!\n");
    }
```
- [ ]  Pada code diatas program dilakukan secara linear karena ada function wait pada parent process.

- Sub_soal 2 -> Menghapus file yang tidak merepresentasikan pemain manchester united.
```sh
c_pid = fork();
    if (c_pid == 0){
        chdir("/home/lihardo04/Desktop/soal3/players");
        char *argv[12] = {"/usr/bin/find", ".", "-type", "f", "!", "-name", "*ManUtd*", "-exec", "/usr/bin/rm", "{}", "+", NULL};
        execv("/usr/bin/find", argv);
    }
    else {
        wait (NULL);
        printf ("delete un-MNU done!\n");
    }
```

- Sub_soal 3 -> Membuat 4 directory sesuai nama posisi pemain bola, yaitu Kiper, Bek, Gelandang, dan Penyerang. serta memindahkan seluruh file .png dari directory players ke directory yang baru dibuat berdasarkan nama posisi mereka masing - masing.
> Pembuatan 4 directory
```sh
c_pid = fork();
    if (c_pid == 0){
        chdir("/home/lihardo04/Desktop/soal3");
        char *argv[2] = {"mkdir", "Kiper"};
        execv("/usr/bin/mkdir", argv);
    }
    else {
        wait (NULL);
        printf ("Creating Dir Kiper done!\n");
    }
      
c_pid = fork();
    if (c_pid == 0){
        chdir("/home/lihardo04/Desktop/soal3");
        char *argv[2] = {"mkdir", "Bek"};
        execv("/usr/bin/mkdir", argv);
    }
    else {
        wait (NULL);
        printf ("Creating Dir Bek done!\n");
    }
      
c_pid = fork();
    if (c_pid == 0){
        chdir("/home/lihardo04/Desktop/soal3");
        char *argv[2] = {"mkdir", "Gelandang"};
        execv("/usr/bin/mkdir", argv);
    }
    else {
        wait (NULL);
        printf ("Creating Dir Gelandang done!\n");
    }
      
c_pid = fork();
    if (c_pid == 0){
        chdir("/home/lihardo04/Desktop/soal3");
        char *argv[2] = {"mkdir", "Penyerang"};
        execv("/usr/bin/mkdir", argv);
    }
    else {
        wait (NULL);
        printf ("Creating Dir Penyerang done!\n");
    }
```
> Menjalankan pengklasifikasian file secara bersamaan
``` sh
       // Create first child process
  pid_t child_id1 = fork();

  if (child_id1 < 0) {
    exit(EXIT_FAILURE); // If the creation of the first child process fails, exit the program
  }

  if (child_id1 == 0) {
    // This is the first child

    char *argv[] = {"/bin/sh", "-c", "mv /home/lihardo04/Desktop/soal3/players/*Kiper* /home/lihardo04/Desktop/soal3/Kiper/", NULL};
    if (execv("/bin/sh", argv) < 0) {
      perror("mv");
      exit(EXIT_FAILURE);
    }
  }

  // Create second child process
  pid_t child_id2 = fork();

  if (child_id2 < 0) {
    exit(EXIT_FAILURE); // If the creation of the second child process fails, exit the program
  }

  if (child_id2 == 0) {
    // This is the second child

    char *argv[] = {"/bin/sh", "-c", "mv /home/lihardo04/Desktop/soal3/players/*Bek* /home/lihardo04/Desktop/soal3/Bek/", NULL};
    if (execv("/bin/sh", argv) < 0) {
      perror("mv");
      exit(EXIT_FAILURE);
    }
  }

  // Create third child process
  pid_t child_id3 = fork();

  if (child_id3 < 0) {
    exit(EXIT_FAILURE); // If the creation of the third child process fails, exit the program
  }

  if (child_id3 == 0) {
    // This is the third child

    char *argv[] = {"/bin/sh", "-c", "mv /home/lihardo04/Desktop/soal3/players/*Gelandang* /home/lihardo04/Desktop/soal3/Gelandang/", NULL};
    if (execv("/bin/sh", argv) < 0) {
      perror("mv");
      exit(EXIT_FAILURE);
    }
  }

  // Run fourth program 
  pid_t child_id4 = fork();
  if (child_id4 < 0) {
    exit(EXIT_FAILURE); // If the creation of the third child process fails, exit the program
  }
  if (child_id4 == 0) {
  char *argv[] = {"/bin/sh", "-c", "mv /home/lihardo04/Desktop/soal3/players/*Penyerang* /home/lihardo04/Desktop/soal3/Penyerang/", NULL};
    if (execv("/bin/sh", argv) < 0) {
    perror("mv");
    exit(EXIT_FAILURE);
  }
}
  
  waitpid(child_id1, NULL, 0);
  waitpid(child_id2, NULL, 0);
  waitpid(child_id3, NULL, 0);
  waitpid(child_id4, NULL, 0);
```
- [ ]  Diakhir kita melakukan wait dari keempat child-process agar program tidak dijalankan sebelum keempat filter selesai dilakukan.

- Sub_soal 4 -> Melakukan pemiilihan pemain dari file .png yang sudah diklasifikasikan berdasarkan posisi mereka di 4 directory yang berbeda. pemilihan tim berdasarkan rating mereka dengan tambahan kondisi dimana kiper hanya terdapat 1 orang. diakhir kita harus men-generate file .txt dengan form Formasi_[jumlah bek]-[jumlah gelandang]-[jumlah penyerang].txt yang dibuat di /home/[users]/ serta dalam file tersebut terdapat nama - nama pemain dari tim yang telah terbentuk. 
**NOTE : semua instruksi diatas harus dilakukan oleh external function dengan nama buat_tim.**
``` sh
typedef struct {
  char Name[50];
  char position[10];
  int rating;
} player;

player arr_player[100];
player tim[11] ={0};
int player_count = 0;

void buatTim(int bek, int gelandang, int striker) {
  printf ("Program running!");
   char *comp_dir[] = {"Bek", "Gelandang", "Kiper", "Penyerang"};  
    char filepath[256];
    player *best_kiper = NULL;
    for (int i = 0; i < sizeof(comp_dir) / sizeof(comp_dir[0]); i++) {
        snprintf(filepath, sizeof(filepath), "/home/lihardo04/Desktop/soal3/%s", comp_dir[i]);
        printf ("\n%s\n", filepath);
        DIR *dir = opendir(filepath);
        if (!dir) {
            printf("Error opening directory %s\n", comp_dir[i]);
            continue;
        }

        struct dirent *ent;
        while ((ent = readdir(dir)) != NULL) {
            if (ent->d_name[0] == '.') continue;  // skip hidden files
            char *name = strtok(ent->d_name, "_");
            char *group = strtok(NULL, "_");
            char *position = strtok(NULL, "_");
            char *rating_str = strtok(NULL, ".");
            printf ("%s %s %s %s\n", name, group, position, rating_str);
            if (name == NULL || group == NULL || position == NULL || rating_str == NULL) {
                printf("Error parsing filename %s\n", ent->d_name);
                continue;
            }            
                player p;
                strcpy(p.Name, name);
                strcpy(p.position, position);
                p.rating = atoi(rating_str);
                arr_player[player_count++] = p; 
        }
      
        closedir(dir);
    }
    
  printf("Found %d players:\n", player_count);
  
  // Find the best goalkeeper and add to the team
    int goalkeeper_index = -1;
    for (int i = 0; i < player_count; i++) {
        if (strcmp(arr_player[i].position, "Kiper") == 0) {
            tim[0] = arr_player[i];
            goalkeeper_index = i;
            break;
        }
    }

    if (goalkeeper_index == -1) {
        printf("Error: no goalkeeper found\n");
        return;
    }

    // Find the 10 best players from the other positions and add to the team
    int bek_count = 0;
    int gelandang_count = 0;
    int penyerang_count = 0;
    int team_index = 1;
    for (int i = 0; i < player_count; i++) {
      if (team_index >=11){
          break;
      }
      else {
        if (i == goalkeeper_index) {
            continue;
        }

        if (strcmp(arr_player[i].position, "Bek") == 0) {
                tim[team_index++] = arr_player[i];
                bek_count++;}
                
        if (strcmp(arr_player[i].position, "Gelandang") == 0) {
                tim[team_index++] = arr_player[i];
                gelandang_count++;}
        if (strcmp(arr_player[i].position, "Penyerang") == 0) {
                tim[team_index++] = arr_player[i];
                penyerang_count++;} 
      }            
    }
    char filename[256];
    snprintf(filename, sizeof(filename), "Formasi_%d-%d-%d.txt", bek_count, gelandang_count, penyerang_count);

    // Create the full file path
    char file_path[256];
    snprintf(file_path, sizeof(file_path),"/home/lihardo04/%.*s", 240-17, filename);

    // Fork a new process
    pid_t pid = fork();
    if (pid == -1) {
        perror("Error forking process");
        exit(1);
    }

    // Child process
    if (pid == 0) {
        // Create an array to hold the arguments for the execv call
        char *args[] = {"touch", file_path, NULL};

        // Call execv to run the touch command
        if (execv("/usr/bin/touch", args) == -1) {
            perror("Error running execv");
            exit(1);
        }
    }
    
    // Parent process
    // Wait for the child process to complete
    int status;
    waitpid(pid, &status, 0);

    // Check if the child process exited normally
    if (WIFEXITED(status)) {
        printf("File created successfully: %s\n", file_path);
    } else {
        printf("Error creating file: %s\n", file_path);
    }
    
    // open file in "append" mode
FILE *fp = fopen(file_path, "a");

// check if file was opened successfully
if (fp == NULL) {
    printf("Error opening file!");
    return;
}

// loop through array and write each element to file
for (int i = 0; i < 11; i++) {
    fprintf(fp, "%s %s %d\n", tim[i].Name, tim[i].position, tim[i].rating);
}

// close the file
fclose(fp);
}
```
- [ ]  Pada code diatas saya melakukan pemilihan pemainnya dengan melakukan sorting dan pengambilan pemain berdasarkan rating dari array yang bertipe data players, dengan kondisi pemain kiper hanya terdapat 1 orang.

<br>
> Sebelum menjalankan program filter.c yang sudah di compile menjadi get.exe
![Screenshot_2023-04-08_133125](/uploads/e039dfc0891dde810e8030797da0a72a/Screenshot_2023-04-08_133125.png)

<br>

<br>
> Setelah menjalankan get.exe
![Screenshot_2023-04-08_133416](/uploads/c88bf0b4e30f3671b0044b52ae0b69b3/Screenshot_2023-04-08_133416.png)
<br>

- [ ]   pada command line di samping telah ditampilkan pemain yang sudah dibentuk berdasarkan rating

<br>
> Text file yang di generate di /home/user/
![Screenshot_2023-04-08_133759](/uploads/22cdf43de397d0f7b0e76aee4e55fc6f/Screenshot_2023-04-08_133759.png)
<br>

<br>
> Isi dari text file
![Screenshot_2023-04-08_133918](/uploads/ebc007a180dfeee5f4840e75cd0e53f5/Screenshot_2023-04-08_133918.png)
<br>

#### Kendala dan Catatan Soal 3
Kendala sempat terjadi dalam pembuatan process yang berlangsung bersamaan dimana agar tidak terjadi saling timpa antar process yang diatasi dengan pembagian 3 PID yang berbeda dan 1 process yang dijalankan oleh parent process.

### Soal 4

- Dalam pelatihan fokus time managementnya, Banabil harus bisa membuat program yang dapat menerima argumen berupa Jam (0-23), Menit (0-59), Detik (0-59), Tanda asterisk [ * ] (value bebas), serta path file .sh.

- Dalam pelatihan fokus untuk ketepatan pilihannya, Banabil ingin programnya dapat mengeluarkan pesan “error” apabila argumen yang diterima program tidak sesuai. Pesan error dapat dibentuk sesuka hati oleh pembuat program. terserah bagaimana, yang penting tulisan error.

```sh
    int cronhour, cronmin, cronsec, status;
    char scronhour[3], scronmin[3], scronsec[3], path[100];
    scanf("%s %s %s %s", scronhour, scronmin, scronsec, path);
    if(strcmp(scronhour, "*")!=0){
        cronhour = atoi(scronhour);
        if(!(cronhour >=0 || cronhour <=23)){
            puts ("Error Hour Argument");
            return 0;
        }
    } else {
        bhour = true;
    }
    if (strcmp(scronmin, "*")!=0){
        cronmin = atoi(scronmin);
        if(!(cronmin >=0 || cronmin <=59)){
            puts ("Error Minute Argument");
            return 0;
        }
    } else{
        bmin = true;
    }
    if (strcmp(scronsec, "*")!=0){
        cronsec = atoi(scronsec);
        if(!(cronsec >=0 || cronsec <=59)){
            puts ("Error Minute Argument");
            return 0;
        }
    } else{
        bsec=true;
    }
```

- [ ] Program akan menerima input melalui scanf. Input akan dicek apakah sudah sesuai atau tidak. Selain itu, input juga akan dibedakan apakah merupakan asterisk atau angka.

- Terakhir, dalam pelatihan kesempurnaan fokusnya, Banabil ingin program ini berjalan dalam background dan hanya menerima satu config cron.

```sh
    pid_t in_pid, in_sid;
    in_pid=fork();
    if (in_pid<0){
        exit(EXIT_FAILURE);
    }
    if(in_pid>0){
        exit(EXIT_SUCCESS);
    }
    umask(0);
    in_sid=setsid();
    if(in_sid<0){
        exit(EXIT_FAILURE);
    }
    if((chdir("/"))<0){
        exit(EXIT_FAILURE);
    }
    close(STDIN_FILENO);
    close(STDERR_FILENO);
    close(STDOUT_FILENO);
    while (1){
        time_t t = time (NULL);
        struct tm tm = *localtime(&t);
        if((tm.tm_hour == cronhour || bhour) && (tm.tm_min == cronmin || bmin) && (tm.tm_sec == cronsec || bsec)){
            pid_t pid;
            pid = fork();
            if(pid==0){
                execl("/bin/bash", "bash", path, NULL);
            } 
        }
        sleep(1);
    }
```
- [ ] Program dibuat menjadi daemon agar dapat berjalan dalam background. Karena scanf hanya terjadi satu kali, program hanya dapat menerima satu config cron. 

- [ ] Dalam daemon dilakukan pengecekan time sekarang. Jika time sesuai dengan config cron maka program akan dijalankan dalam child baru.
