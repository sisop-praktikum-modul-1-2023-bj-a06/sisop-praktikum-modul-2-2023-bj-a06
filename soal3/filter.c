#include <stdio.h>
#include <unistd.h>
#include <sys/wait.h>
#include <stdlib.h>
#include <string.h>
#include <dirent.h>

typedef struct {
  char Name[50];
  char position[10];
  int rating;
} player;

player arr_player[100];
player tim[11] ={0};
int player_count = 0;

void buatTim(int bek, int gelandang, int striker) {
  printf ("Program running!");
   char *comp_dir[] = {"Bek", "Gelandang", "Kiper", "Penyerang"};  
    char filepath[256];
    player *best_kiper = NULL;
    for (int i = 0; i < sizeof(comp_dir) / sizeof(comp_dir[0]); i++) {
        snprintf(filepath, sizeof(filepath), "/home/lihardo04/Desktop/soal3/%s", comp_dir[i]);
        printf ("\n%s\n", filepath);
        DIR *dir = opendir(filepath);
        if (!dir) {
            printf("Error opening directory %s\n", comp_dir[i]);
            continue;
        }

        struct dirent *ent;
        while ((ent = readdir(dir)) != NULL) {
            if (ent->d_name[0] == '.') continue;  // skip hidden files
            char *name = strtok(ent->d_name, "_");
            char *group = strtok(NULL, "_");
            char *position = strtok(NULL, "_");
            char *rating_str = strtok(NULL, ".");
            printf ("%s %s %s %s\n", name, group, position, rating_str);
            if (name == NULL || group == NULL || position == NULL || rating_str == NULL) {
                printf("Error parsing filename %s\n", ent->d_name);
                continue;
            }            
                player p;
                strcpy(p.Name, name);
                strcpy(p.position, position);
                p.rating = atoi(rating_str);
                arr_player[player_count++] = p; 
        }
      
        closedir(dir);
    }
    
  printf("Found %d players:\n", player_count);
  
  // Find the best goalkeeper and add to the team
    int goalkeeper_index = -1;
    for (int i = 0; i < player_count; i++) {
        if (strcmp(arr_player[i].position, "Kiper") == 0) {
            tim[0] = arr_player[i];
            goalkeeper_index = i;
            break;
        }
    }

    if (goalkeeper_index == -1) {
        printf("Error: no goalkeeper found\n");
        return;
    }

    // Find the 10 best players from the other positions and add to the team
    int bek_count = 0;
    int gelandang_count = 0;
    int penyerang_count = 0;
    int team_index = 1;
    for (int i = 0; i < player_count; i++) {
      if (team_index >=11){
          break;
      }
      else {
        if (i == goalkeeper_index) {
            continue;
        }

        if (strcmp(arr_player[i].position, "Bek") == 0) {
                tim[team_index++] = arr_player[i];
                bek_count++;}
                
        if (strcmp(arr_player[i].position, "Gelandang") == 0) {
                tim[team_index++] = arr_player[i];
                gelandang_count++;}
        if (strcmp(arr_player[i].position, "Penyerang") == 0) {
                tim[team_index++] = arr_player[i];
                penyerang_count++;} 
      }            
    }
    char filename[256];
    snprintf(filename, sizeof(filename), "Formasi_%d-%d-%d.txt", bek_count, gelandang_count, penyerang_count);

    // Create the full file path
    char file_path[256];
    snprintf(file_path, sizeof(file_path),"/home/lihardo04/%.*s", 240-17, filename);

    // Fork a new process
    pid_t pid = fork();
    if (pid == -1) {
        perror("Error forking process");
        exit(1);
    }

    // Child process
    if (pid == 0) {
        // Create an array to hold the arguments for the execv call
        char *args[] = {"touch", file_path, NULL};

        // Call execv to run the touch command
        if (execv("/usr/bin/touch", args) == -1) {
            perror("Error running execv");
            exit(1);
        }
    }
    
    // Parent process
    // Wait for the child process to complete
    int status;
    waitpid(pid, &status, 0);

    // Check if the child process exited normally
    if (WIFEXITED(status)) {
        printf("File created successfully: %s\n", file_path);
    } else {
        printf("Error creating file: %s\n", file_path);
    }
    
    // open file in "append" mode
FILE *fp = fopen(file_path, "a");

// check if file was opened successfully
if (fp == NULL) {
    printf("Error opening file!");
    return;
}

// loop through array and write each element to file
for (int i = 0; i < 11; i++) {
    fprintf(fp, "%s %s %d\n", tim[i].Name, tim[i].position, tim[i].rating);
}

// close the file
fclose(fp);
}

int main (){
  pid_t c_pid = fork();
    if (c_pid ==0){
    char *argv[8] = {"wget","--no-check-certificate","-P","/home/lihardo04/Desktop/soal3","-O","players.zip", "https://drive.google.com/uc?export=download&id=1zEAneJ1-0sOgt13R1gL4i1ONWfKAtwBF", NULL};
    execv("/usr/bin/wget", argv);
      }
    else {
    wait(NULL);
    printf("File downloaded successfully!\n");
    }
    
    c_pid = fork();
    if (c_pid ==0) {
      char *argv[6] = {"unzip", "players.zip", "players/*","-O","players", NULL};
      execv("/usr/bin/unzip", argv);
    }
    else {
      wait (NULL);
      printf ("Unzip file done!\n");
    }
    
    c_pid = fork();
          if (c_pid == 0){
              char *argv[4] = {"rm","-r", "players.zip",NULL};
              execv("/usr/bin/rm", argv);
          }
          else {
            wait (NULL);
            printf ("delete directory done!\n");
      }
      
      c_pid = fork();
          if (c_pid == 0){
              chdir("/home/lihardo04/Desktop/soal3/players");
              char *argv[12] = {"/usr/bin/find", ".", "-type", "f", "!", "-name", "*ManUtd*", "-exec", "/usr/bin/rm", "{}", "+", NULL};
        execv("/usr/bin/find", argv);
          }
          else {
            wait (NULL);
            printf ("delete un-MNU done!\n");
      }
      c_pid = fork();
          if (c_pid == 0){
              chdir("/home/lihardo04/Desktop/soal3");
              char *argv[2] = {"mkdir", "Kiper"};
        execv("/usr/bin/mkdir", argv);
          }
          else {
            wait (NULL);
            printf ("Creating Dir Kiper done!\n");
      }
      
      c_pid = fork();
          if (c_pid == 0){
              chdir("/home/lihardo04/Desktop/soal3");
              char *argv[2] = {"mkdir", "Bek"};
        execv("/usr/bin/mkdir", argv);
          }
          else {
            wait (NULL);
            printf ("Creating Dir Bek done!\n");
      }
      
      c_pid = fork();
          if (c_pid == 0){
              chdir("/home/lihardo04/Desktop/soal3");
              char *argv[2] = {"mkdir", "Gelandang"};
        execv("/usr/bin/mkdir", argv);
          }
          else {
            wait (NULL);
            printf ("Creating Dir Gelandang done!\n");
      }
      
      c_pid = fork();
          if (c_pid == 0){
              chdir("/home/lihardo04/Desktop/soal3");
              char *argv[2] = {"mkdir", "Penyerang"};
        execv("/usr/bin/mkdir", argv);
          }
          else {
            wait (NULL);
            printf ("Creating Dir Penyerang done!\n");
      }
      
       // Create first child process
  pid_t child_id1 = fork();

  if (child_id1 < 0) {
    exit(EXIT_FAILURE); // If the creation of the first child process fails, exit the program
  }

  if (child_id1 == 0) {
    // This is the first child

    char *argv[] = {"/bin/sh", "-c", "mv /home/lihardo04/Desktop/soal3/players/*Kiper* /home/lihardo04/Desktop/soal3/Kiper/", NULL};
    if (execv("/bin/sh", argv) < 0) {
      perror("mv");
      exit(EXIT_FAILURE);
    }
  }

  // Create second child process
  pid_t child_id2 = fork();

  if (child_id2 < 0) {
    exit(EXIT_FAILURE); // If the creation of the second child process fails, exit the program
  }

  if (child_id2 == 0) {
    // This is the second child

    char *argv[] = {"/bin/sh", "-c", "mv /home/lihardo04/Desktop/soal3/players/*Bek* /home/lihardo04/Desktop/soal3/Bek/", NULL};
    if (execv("/bin/sh", argv) < 0) {
      perror("mv");
      exit(EXIT_FAILURE);
    }
  }

  // Create third child process
  pid_t child_id3 = fork();

  if (child_id3 < 0) {
    exit(EXIT_FAILURE); // If the creation of the third child process fails, exit the program
  }

  if (child_id3 == 0) {
    // This is the third child

    char *argv[] = {"/bin/sh", "-c", "mv /home/lihardo04/Desktop/soal3/players/*Gelandang* /home/lihardo04/Desktop/soal3/Gelandang/", NULL};
    if (execv("/bin/sh", argv) < 0) {
      perror("mv");
      exit(EXIT_FAILURE);
    }
  }

  // Run fourth program 
  pid_t child_id4 = fork();
  if (child_id4 < 0) {
    exit(EXIT_FAILURE); // If the creation of the third child process fails, exit the program
  }
  if (child_id4 == 0) {
  char *argv[] = {"/bin/sh", "-c", "mv /home/lihardo04/Desktop/soal3/players/*Penyerang* /home/lihardo04/Desktop/soal3/Penyerang/", NULL};
    if (execv("/bin/sh", argv) < 0) {
    perror("mv");
    exit(EXIT_FAILURE);
  }
}
  
  waitpid(child_id1, NULL, 0);
  waitpid(child_id2, NULL, 0);
  waitpid(child_id3, NULL, 0);
  waitpid(child_id4, NULL, 0);
  
  buatTim(8,7,2);
  
  for (int i = 0; i < player_count; i++) {
        printf("Player %d: %s (%s) - Rating: %d\n", i+1, arr_player[i].Name, arr_player[i].position, arr_player[i].rating);
    }
  
    for (int i=0;i<12;i++){
      printf ("\n%s %s %d", tim[i].Name, tim[i].position, tim[i].rating);
    }
    return 0;
  }


