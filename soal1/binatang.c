#include <stdio.h>
#include <unistd.h>
#include <sys/wait.h>
#include <stdlib.h>
#include <string.h>

int main() {
    pid_t c_pid = fork();
    if (c_pid ==0){
    char *argv[7] = {"wget","-P","/home/lihardo04/Desktop/soal1","-O","binatang.zip", "https://drive.google.com/uc?       export=download&id=1oDgj5kSiDO0tlyS7-20uz7t20X3atwrq", NULL};
    execv("/usr/bin/wget", argv);
      }
    else {
    wait(NULL);
    printf("File downloaded successfully!\n");
    }
    
    c_pid = fork();
    if (c_pid ==0) {
      char *argv[5] = {"unzip", "binatang.zip", "-d", "binatang", NULL};
      execv("/usr/bin/unzip", argv);
    }
    else {
      wait (NULL);
      printf ("Unzip file done!\n");
    }
    
    // Select a random file and copy it to destination directory
    char command[256];  
    sprintf(command, "cp \"%s\" /home/lihardo04/Desktop/soal1/current_u_spv/", "$(shuf -n 1 -e /home/lihardo04/Desktop/soal1/binatang/*)");
    strcat(command, "/");
    strcat(command, "$(basename \"$command\")");
    int ret = system(command);
    if (ret == -1) {
        perror("system failed");
        exit(EXIT_FAILURE); 
    } else if (WEXITSTATUS(ret) != 0) {
        printf("Command failed\n");
    } else {
        printf("Command succeeded\n");
    }
    
    system ("mkdir /home/lihardo04/Desktop/soal1/HewanDarat");
    system ("mkdir /home/lihardo04/Desktop/soal1/HewanAmphibi");
    system ("mkdir /home/lihardo04/Desktop/soal1/HewanAir");
    system("find /home/lihardo04/Desktop/soal1/binatang -type f -name \"*air*\" -exec mv {} /home/lihardo04/Desktop/soal1/HewanAir \\;");
    system("find /home/lihardo04/Desktop/soal1/binatang -type f -name \"*darat*\" -exec mv {} /home/lihardo04/Desktop/soal1/HewanDarat \\;");
    system("find /home/lihardo04/Desktop/soal1/binatang -type f -name \"*amphibi*\" -exec mv {} /home/lihardo04/Desktop/soal1/HewanAmphibi \\;");
    
    
    
    for (int i = 1; i < 4; i++) {
        char key_word[30];
        if (i == 1) {
            strcpy(key_word, "HewanAir");
        }
        else if (i == 2) {
            strcpy(key_word, "HewanDarat");
        }
        else if (i == 3) {
            strcpy(key_word, "HewanAmphibi");
        }

        char zip_name[40];
        sprintf(zip_name, "%s.zip", key_word);

        c_pid = fork();
        if (c_pid == 0) {
            char *argv[10] = {"zip", "-r", zip_name, key_word, "-jr","-x", "*.zip", "-x", ".", NULL};
            chdir("/home/lihardo04/Desktop/soal1");  // Change working directory to the parent directory
            execv("/usr/bin/zip", argv);
        }
        else {
            int status;
            waitpid(c_pid, &status, 0);
            if (WIFEXITED(status) && WEXITSTATUS(status) == 0) {
                printf("%s file done!\n", zip_name);
            } else {
                fprintf(stderr, "zip error: %s\n", zip_name);
            }
        }
        
        chdir("/home/lihardo04/Desktop/soal1");
        c_pid = fork();
          if (c_pid == 0){
              char *argv[4] = {"rm","-r", key_word,NULL};
              execv("/usr/bin/rm", argv);
          }
          else {
            wait (NULL);
            printf ("delete directory done!\n");
      }
    }
    system ("rm -r binatang");
    return 0;
}
